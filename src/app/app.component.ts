import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'drag';
  windows: Object[] = [];

  edge = {
    top: true,
    bottom: true,
    left: true,
    right: true
  };

   addJanela() {
    this.windows.push({
      id: this.windows.length
    });
  }

  fecharJanela(id) {
    console.log(id);
    const windowIndex = this.windows.findIndex((window) => (window.id === id));
    console.log(windowIndex);
    this.windows.splice(windowIndex, windowIndex);
  }

}
